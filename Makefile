CC:= avr-gcc
BOARD_TAG = attiny85at8
F_CPU = 1000000L
ISP_PROG = usbtiny

prog.hex: prog.elf
	avr-objcopy -j .text -j .data -O ihex prog.elf prog.hex

prog.elf: main.c
	$(CC) main.c -o prog.elf -O2 -D F_CPU=1000000 -mmcu=attiny85

.PHONY: flash

flash: prog.hex
	avrdude -p t85 -c usbtiny -U flash:w:prog.hex:a
