#include <avr/io.h>
#include <util/delay_basic.h>
#include <stdbool.h>

#define PULSE_MIN 250
#define PULSE_MAX 500
#define PULSE_CHANGE_SPEED 1

// Pins used:
// Pin 2 (PB3)
// Pin 3 (PB4)

static inline void output(uint16_t pulse) {
    // pulse is pulse length for PB3
    // PB4 is oposite, so PULSE_MAX + PULSE_MIN - pulse
    PORTB = 0;
    _delay_loop_2(5000); // Around 20 ms
    PORTB = (1<<PORTB3);
    _delay_loop_2(pulse);
    PORTB = (1<<PORTB4);
    _delay_loop_2(PULSE_MAX + PULSE_MIN - pulse);
    PORTB = 0;
}

void main() {
    DDRB = (1<<DDB3)|(1<<DDB4); // Set PB3 and PB4 as outputs

    uint16_t pulse = PULSE_MIN;

    while(true) {
        for (;pulse <= PULSE_MAX;pulse+=PULSE_CHANGE_SPEED){
            output(pulse);
        }
        for (;pulse >= PULSE_MIN;pulse-=PULSE_CHANGE_SPEED){
            output(pulse);
        }

    }
}
